<?php

namespace Drupal\Tests\xnttstrjp\Unit;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\xnttstrjp\Plugin\ExternalEntities\PropertyMapper\StringJsonPath;

/**
 * The class to test StringJsonPath.
 *
 * @group xnttstrjp
 */
class StringJsonPathTest extends UnitTestCase {

  /**
   * Data provider for testExtractValueFromRawDataUsingMapping().
   */
  public function provideTestExtractValueFromRawDataUsingMapping() {
    return [
      // Empty value.
      [[], ['bla' => 4], 'mapping' => ''],
      // Static string value.
      [['toto'], ['bli' => 4], '"toto"'],
      // Static integer value.
      [[42], ['bli' => 4], '+42'],
      [[-42], [], '-42'],
      // Concate.
      [["blibla"], [], "'bli' . \"bla\""],
      // Basic JSON Path.
      [['2'], ['bla' => '2'], '$.bla'],
      [['3'], ['bla' => '3'], '$[\'bla\']'],
      // Basic String function.
      [
        ['bliblubla'],
        [],
        "substr('blobliblubla', 3)",
      ],
      [
        ['bliblu'],
        [],
        "substr('blobliblubla', 3, 6)",
      ],
      // JSON Path and functions combination.
      [
        ['blablabla'],
        [
          'foo' => [
            'bar' => [42, 'bloblibluble'],
          ],
          'baz' => 'bla',
        ],
        "substr(str_replace('obli', 'abla', $.foo.bar.1), 0, strpos($.foo.bar.1, 'blu')) . $.baz",
      ],
    ];
  }

  /**
   * Test value extraction from raw data using given mappings.
   *
   * @dataProvider provideTestExtractValueFromRawDataUsingMapping
   */
  public function testExtractValueFromRawDataUsingMapping($expected, $data, $mapping) {
    $configuration = [];
    $plugin_id = 'xnttstrjp';
    $plugin_definition = [];

    $logger = $this->createMock(LoggerChannelInterface::class);
    $logger->expects($this->any())
      ->method('error');
    // ->willReturn(NULL);
    $logger_factory = $this->createMock(LoggerChannelFactoryInterface::class);
    $logger_factory
      ->expects($this->any())
      ->method('get')
      ->with('xntt_property_mapper_xnttstrjp')
      ->willReturn($logger);

    $entity_type_manager = $this->createMock(EntityTypeManager::class);
    $entity_field_manager = $this->createMock(EntityFieldManagerInterface::class);
    $string_translation = $this->createMock(TranslationInterface::class);
    $data_processor_manager = $this->createMock(PluginManagerInterface::class);
    $strjpath = new StringJsonPath(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $string_translation,
      $logger_factory,
      $entity_field_manager,
      $data_processor_manager
    );
    $value = $strjpath->extractValueFromRawDataUsingMapping($data, $mapping);
    $this->assertEquals($expected, $value, 'Failed with ');
  }

  /**
   * Data provider for testInvalidExtractValueFromRawDataUsingMapping().
   */
  public function provideTestInvalidExtractValueFromRawDataUsingMapping() {
    return [
      // Invalid mapping syntax.
      [[], [], '--'],
      [[], [], 'toto'],
      [[], [], '"toto""'],
      [[], [], '..'],
      // Invalid JSON Path.
      [[], [], '$...'],
      [[], ['bla' => 806], '$.bli'],
      // Invalid function.
      [[], ['bla' => 806], 'bli(42)'],
      // Function with avalid arguments.
      [[], ['bla' => 806], 'substr()'],
    ];
    // @todo Add test for field names containing spaces and other special
    // characters.
  }

  /**
   * Test value extraction failures.
   *
   * @dataProvider provideTestInvalidExtractValueFromRawDataUsingMapping
   */
  public function testInvalidExtractValueFromRawDataUsingMapping($expected, $data, $mapping) {
    $configuration = [];
    $plugin_id = 'xnttstrjp';
    $plugin_definition = [];
    $logger = $this->getMockBuilder(LoggerChannelInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $logger->expects($this->any())
      ->method('error');
    // ->willReturn(NULL);
    $logger_factory = $this->getMockBuilder(LoggerChannelFactoryInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $logger_factory
      ->expects($this->any())
      ->method('get')
      ->with('xntt_property_mapper_xnttstrjp')
      ->willReturn($logger);
    $entity_type_manager = $this->createMock(EntityTypeManager::class);
    $entity_field_manager = $this->createMock(EntityFieldManagerInterface::class);
    $string_translation = $this->createMock(TranslationInterface::class);
    $data_processor_manager = $this->createMock(PluginManagerInterface::class);
    $strjpath = new StringJsonPath(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $string_translation,
      $logger_factory,
      $entity_field_manager,
      $data_processor_manager
    );
    $value = $strjpath->extractValueFromRawDataUsingMapping($data, $mapping);
    $this->assertEquals($expected, $value, 'Failed with ');
  }

  /**
   * Test available PHP functions in expressions.
   */
  public function testFunctionsInExtractValueFromRawDataUsingMapping() {
    // @todo Test all string functions.
    $this->markTestIncomplete(
      'This test has not been implemented yet.'
    );
  }

}
