External Entities String Functions and JSONPath Field Mapper plugin
*******************************************************************

Plugin for External Entities that provides a field mapper supporting string
functions and JSONPath.

===============================

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Description
 * Syntax
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

This modules enables the combination of string functions and JSONPath to map
external entity field values but also supports simple field mapping. To map
external entity fields to Drupal fields, it is possible to use either:
  - a simple field mapping by providing the field name (and nothing else);
  - a constant (static) value by prefixing the value with "+" or by using quotes
    for strings or just using valid integer values alone;
  - a JSONPath expression that begins with "$";
  - one of the allowed/supported PHP string functions with its arguments that
    could be a constant (quoted string or integer), a JSONPath or the result of
    another PHP string function call.

DESCRIPTION
-----------

With the default field mapping plugins, it is not possible to use parts of
multiple field values to generate a new one. However, in some cases, it might be
convenient to do so.

For instance it could be convenient to process a value to remove capital
letters, replace or remove some sub-strings, compute a CRC or a hash, etc.
When using xnttmulti to aggregate multiple sources into one external entity
type, it might be convenient to be able to reference other entities according to
the initial source of a given instance.

Let's say I have an external entity "aggreg" that uses 2 REST service sources
(alpha and beta) using xnttmulti: source "alpha" and "beta" provide the same
field values but are differentiated by a prefix added to their identifier (using
xnttmulti features). Therefore, on alpha and beta REST services, I could find an
entity using the id "123" but from Drupal external entity "aggreg", I would have
2 entities, one with the identifier "a123" and another with "b123". So far, so
good. But now, if those entities have a reference to an other entity, for
instance a "child" entity, it would use the identifier of the source. If alpha
"123" entity has a child "456", the corresponding Drupal external entity aggreg
"a123" would just have a "456" identifier without prefix. Impossible to tell if
it refers to "alpha" or "beta". This plugin can be used to solve that case by
using a sub-string of the entity identifier that would extract the "source
prefix" (ie. "a" or "b") and prefix it to the child reference field, so we would
have "a456" that would refer to the correct source.

SYNTAX
------

Here is the supported field mapping syntax:
  
  [ field_name | expression | +constant ]
  
Where:
  expression = function_name(arguments) | int | string | JSONPath |
               expression . expression
  arguments = expression [, expression [, ...]]
  Note: the dot "." operator is used to concatenate expression values.

  A "field_name" is a field name of the raw external data, starting by an
  alphabetic character and containing only alpha-numeric characters,
  underscores, dashes and dots.
  "int" stands for integer value.
  "string" stands for single or double quoted string value. Quotes and backslash
  can be escaped using a backslash.
  "function_name" can be any of (see corresponding PHP documentation for usage):

    * addcslashes
    * addslashes
    * bin2hex
    * chr
    * chunk_split
    * convert_uudecode
    * convert_uuencode
    * count_chars
    * crc32
    * crypt
    * explode
    * hebrev
    * hex2bin
    * html_entity_decode
    * htmlentities
    * htmlspecialchars_decode
    * htmlspecialchars
    * implode
    * lcfirst
    * ltrim
    * md5
    * metaphone
    * nl2br
    * number_format
    * quoted_printable_encode
    * quotemeta
    * rtrim
    * sha1
    * soundex
    * sprintf
    * str_getcsv
    * str_ireplace
    * str_pad
    * str_repeat
    * str_replace
    * str_rot13
    * str_shuffle
    * str_split
    * strip_tags
    * stripcslashes
    * stripslashes
    * stristr
    * strpos
    * strrev
    * strtok
    * strtolower
    * strtoupper
    * strtr
    * substr_replace
    * substr
    * trim
    * ucfirst
    * ucwords
    * utf8_decode
    * utf8_encode
    * vsprintf
    * wordwrap
    * preg_filter
    * preg_grep_keys
    * preg_match_all
    * preg_match
    * preg_quote
    * preg_replace
    * preg_split
  
  Example:

    substr(str_replace("toto", "tutu\'", $.foo.bar.1), 0,
    strpos($.foo.bar.1, 'tata')) . $['baz']



REQUIREMENTS
------------

This module requires the following modules:

 * [External Entities](https://www.drupal.org/project/external_entities)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

The module has no menu or modifiable global settings. There is no configuration.
When enabled, the module will add a new field mapper for external entity
types. Then, when you create a new external entity type, you can select the
"String functions and JSONPath" field mapper plugin and use its syntax for
field mapping.

See the "SYNTAX" section for field mapping synthax.

MAINTAINERS
-----------

Current maintainers:
 * Valentin Guignon (guignonv) - https://www.drupal.org/u/guignonv

Contributors:
 *  Urvashi Vora (urvashi_vora) - https://www.drupal.org/u/urvashi_vora
