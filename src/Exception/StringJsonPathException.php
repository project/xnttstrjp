<?php

namespace Drupal\xnttstrjp\Exception;

/**
 * Generic (parent) exception thrown for String Functions and JSONPath.
 */
class StringJsonPathException extends \RuntimeException {}
