<?php

namespace Drupal\xnttstrjp\Exception;

/**
 * Exception thrown for String Functions and JSONPath syntax errors.
 */
class StringJsonPathSyntaxErrorException extends StringJsonPathException {}
