<?php

namespace Drupal\xnttstrjp\Plugin\ExternalEntities\PropertyMapper;

use Drupal\Core\Form\FormStateInterface;
use Drupal\external_entities\FieldMapper\FieldMapperInterface;
use Drupal\external_entities\Plugin\ExternalEntities\PropertyMapper\JsonPath as PMJsonPath;
use Drupal\xnttstrjp\Exception\StringJsonPathException;
use Drupal\xnttstrjp\Exception\StringJsonPathSyntaxErrorException as SyntaxError;
use JsonPath\InvalidJsonException;
use JsonPath\InvalidJsonPathException;
use JsonPath\JsonObject;

/**
 * A property mapper that uses JSONPath expressions.
 *
 * Multi-valued fields should be mapped using JSONPath expressions that result
 * in an array of values being returned.
 *
 * Constants (fixed values) can be mapped by prefixing the mapping expression
 * with a '+' character.
 *
 * @PropertyMapper(
 *   id = "stringjsonpath",
 *   label = @Translation("String functions and JSONPath"),
 *   description = @Translation("Maps a property based on string functions and JSONPath expressions.")
 * )
 */
class StringJsonPath extends PMJsonPath {

  // @todo improve regex to match complex expressions.
  public const JSON_PATH_REGEX = '\$(?:\.?\*|\.\.\w*|\.\w+|\[\'\w[^\']+\'(?:\s*,\s*\'\w[^\']+\')*\]|\[-?\d+(?:\s*,\s*-?\d+)*\]|\[-?\d*:-?\d*\]|\[\*\])+';
  public const QUOTE_DETECT_REGEX = '#^\s*"(?:(?<=[^\\\\]\\\\)(\\\\\\\\)*"|[^"])*"\s*$|^\s*\'(?:(?<=[^\\\\]\\\\)(\\\\\\\\)*\'|[^\'])*\'\s*$#';

  /**
   * Available string functions.
   *
   * @var array
   */
  protected static $availableStrFunc = [
    'addcslashes'           => TRUE,
    'addslashes'            => TRUE,
    'array'                 => TRUE,
    'array_change_key_case' => TRUE,
    'array_chunk'           => TRUE,
    'array_column'          => TRUE,
    'array_combine'         => TRUE,
    'array_count_values'    => TRUE,
    'array_diff_assoc'      => TRUE,
    'array_diff_key'        => TRUE,
    'array_diff'            => TRUE,
    'array_fill_keys'       => TRUE,
    'array_fill'            => TRUE,
    'array_flip'            => TRUE,
    'array_intersect_assoc' => TRUE,
    'array_intersect_key'   => TRUE,
    'array_intersect'       => TRUE,
    'array_is_list'         => TRUE,
    'array_key_exists'      => TRUE,
    'array_key_first'       => TRUE,
    'array_key_last'        => TRUE,
    'array_keys'            => TRUE,
    'array_merge_recursive' => TRUE,
    'array_merge'           => TRUE,
    'array_multisort'       => TRUE,
    'array_pad'             => TRUE,
    'array_pop'             => TRUE,
    'array_product'         => TRUE,
    'array_rand'            => TRUE,
    'array_replace_recursive' => TRUE,
    'array_replace'         => TRUE,
    'array_reverse'         => TRUE,
    'array_search'          => TRUE,
    'array_shift'           => TRUE,
    'array_slice'           => TRUE,
    'array_splice'          => TRUE,
    'array_sum'             => TRUE,
    'array_unique'          => TRUE,
    'array_values'          => TRUE,
    'bin2hex'               => TRUE,
    'chr'                   => TRUE,
    'chunk_split'           => TRUE,
    'convert_uudecode'      => TRUE,
    'convert_uuencode'      => TRUE,
    'count'                 => TRUE,
    'count_chars'           => TRUE,
    'crc32'                 => TRUE,
    'crypt'                 => TRUE,
    'current'               => TRUE,
    'end'                   => TRUE,
    'explode'               => TRUE,
    'hebrev'                => TRUE,
    'hex2bin'               => TRUE,
    'html_entity_decode'    => TRUE,
    'htmlentities'          => TRUE,
    'htmlspecialchars_decode' => TRUE,
    'htmlspecialchars'      => TRUE,
    'implode'               => TRUE,
    'in_array'              => TRUE,
    'key'                   => TRUE,
    'lcfirst'               => TRUE,
    'ltrim'                 => TRUE,
    'md5'                   => TRUE,
    'metaphone'             => TRUE,
    'nl2br'                 => TRUE,
    'number_format'         => TRUE,
    'quoted_printable_encode' => TRUE,
    'quoted_printable_encode' => TRUE,
    'quotemeta'             => TRUE,
    'range'                 => TRUE,
    'reset'                 => TRUE,
    'rtrim'                 => TRUE,
    'sha1'                  => TRUE,
    'shuffle'               => TRUE,
    'soundex'               => TRUE,
    'sprintf'               => TRUE,
    'str_getcsv'            => TRUE,
    'str_ireplace'          => TRUE,
    'str_pad'               => TRUE,
    'str_repeat'            => TRUE,
    'str_replace'           => TRUE,
    'str_rot13'             => TRUE,
    'str_shuffle'           => TRUE,
    'str_split'             => TRUE,
    'strip_tags'            => TRUE,
    'stripcslashes'         => TRUE,
    'stripslashes'          => TRUE,
    'stristr'               => TRUE,
    'strpos'                => TRUE,
    'strrev'                => TRUE,
    'strtok'                => TRUE,
    'strtolower'            => TRUE,
    'strtotime'             => TRUE,
    'strtoupper'            => TRUE,
    'strtr'                 => TRUE,
    'substr_replace'        => TRUE,
    'substr'                => TRUE,
    'trim'                  => TRUE,
    'ucfirst'               => TRUE,
    'ucwords'               => TRUE,
    'utf8_decode'           => TRUE,
    'utf8_encode'           => TRUE,
    'vsprintf'              => TRUE,
    'wordwrap'              => TRUE,
    'preg_filter'           => TRUE,
    'preg_grep_keys'        => TRUE,
    'preg_match_all'        => TRUE,
    'preg_match'            => TRUE,
    'preg_quote'            => TRUE,
    'preg_replace'          => TRUE,
    'preg_split'            => TRUE,
    // URL functions.
    'base64_decode'         => TRUE,
    'base64_encode'         => TRUE,
    'parse_url'             => TRUE,
    'rawurldecode'          => TRUE,
    'rawurlencode'          => TRUE,
    'urldecode'             => TRUE,
    'urlencode'             => TRUE,
    // @todo maybe add multibyte string functions as well?
    // https://www.php.net/manual/en/ref.mbstring.php
  ];

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {

    $form += parent::buildConfigurationForm($form, $form_state);
    unset($form['data_processors']);

    // @todo Provide reverse mapping form (multiple sets of 2 textfields
    // [raw field name]: [reverse mapping expression]) and manage it in a
    // ExpressionFieldMapperBase::createRawDataFromEntityValues() override.
    $form['help']['syntax'] = [
      '#type' => 'markup',
      '#markup' => $this->t(
        '<p>Supported syntax: <code><i>field_name</i> | "<i>field_name</i>" | <i>expression</i></code><br/><br/>Where "<i>field_name</i>" is a field name of the raw external data (starting by a alphabetic character and containing only alpha-numeric characters, underscores, dashes and dots), "<i>int</i>" stands for integer value, "<i>string</i>" for single or double quoted string value and:<br/><code><strong>expression</strong> = <i>function_name</i>(<i>arguments</i>) | <i>int</i> | <i>string</i> | <i>JSONPath</i> | <i>expression</i> . <i>expression</i></code><br/><code><strong>arguments</strong> = <i>expression</i> [, <i>expression</i> [, <i>...</i>]]</code><br/><br/><br/>Example: <code>substr(str_replace("toto", "tutu\\\'", $.foo.bar.1), 0, strpos($.foo.bar.1, \'tata\')) . $[\'baz\']</code></p>'
      ),
    ];
    $form['help']['strfunc'] = [
      '#type' => 'details',
      '#title' => $this->t('Supported "function_name" list'),
      '#open' => FALSE,
      'strfunclist' => [
        '#theme' => 'item_list',
        '#type' => 'ul',
        '#prefix' => '<p>' . $this->t(
          '(see corresponding PHP documentation for usage)')
        . '</p>',
        '#items' => array_map(
          function ($x) {
            return [
              '#markup' => '<a href="https://www.php.net/manual/en/function.'
              . $x
              . '.php">'
              . $x
              . '</a>',
            ];
          },
          array_keys(static::$availableStrFunc)
        ),
      ],
    ];

    $form['help']['constant'] = [
      '#markup' => "<p></p>",
      '#weight' => 100,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getMappedSourceFieldName() :?string {
    $source_field = NULL;
    // Field or simple JSONPath mapping.
    if (preg_match('/^([^\s(),]+)$/', $this->getConfiguration()['mapping'], $matches)
      || preg_match('/^\$\.(\w+)$/', $this->getConfiguration()['mapping'], $matches)
      || preg_match('/^\$\[[\'"]?(\w+)[\'"]?\]$/', $this->getConfiguration()['mapping'], $matches)
    ) {
      $source_field = $matches[1];
    }
    return $source_field;
  }

  /**
   * Process a mapping on given data to extract the corresponding value.
   *
   * @param array $raw_data
   *   The raw data structure.
   * @param string $mapping
   *   The mapping expression.
   * @param array &$context
   *   The array of context.
   *
   * @return array
   *   The extracted value in an array.
   */
  public function extractValueFromRawDataUsingMapping(
    array $raw_data,
    string $mapping,
    array &$context = []
  ) :array {
    if (empty($mapping)) {
      return [];
    }

    try {
      // Extract each JSONPath from the mapping and replace it by its value.
      if (preg_match_all('#' . static::JSON_PATH_REGEX . '#', $mapping, $matches)) {
        if (empty($context['jsonpath_object'])) {
          $context['jsonpath_object'] = new JsonObject($raw_data, TRUE);
        }
        $json_object = $context['jsonpath_object'];
        foreach ($matches[0] as $match) {
          try {
            $jpath_value = $json_object->get($match);
            if (FALSE === $jpath_value) {
              // No path matches.
              $this->logger->warning('JSONPath failed: @jsonpath', ['@jsonpath' => $match]);
              $jpath_value = NULL;
            }
            // Convert JSONPath mapping result into a PHP variable.
            $tr_value = $this->translateJsonToExpression($jpath_value);
            $mapping = str_replace($match, $tr_value, $mapping);
          }
          catch (InvalidJsonException | InvalidJsonPathException $e) {
            // JSONPath mapping failed. Report.
            throw new StringJsonPathException(
              'Invalid JSONPath: '
              . $e
              . "\nJSONPath: "
              . print_r($match, TRUE)
              . "\nMapping: "
              . print_r($mapping, TRUE)
              . "\nData: "
              . print_r($raw_data, TRUE)
            );
          }
        }
      }
      // Then, process string functions.
      $strpos = 0;
      $func_stack = [];
      $func_concate_stack = [];
      $func_args_stack = [[]];
      $expect_separator = FALSE;
      $concate = FALSE;
      while ($strpos < strlen($mapping)) {
        $new_value = NULL;
        // Find the next element type.
        if (ctype_alpha($mapping[$strpos]) || ('_' == $mapping[$strpos])) {
          // A function name, get the name.
          $func = $mapping[$strpos];
          ++$strpos;
          while (($strpos < strlen($mapping))
              && ((ctype_alpha($mapping[$strpos])
                || (ctype_digit($mapping[$strpos]))
                || ('_' == $mapping[$strpos]))
              )
          ) {
            $func .= $mapping[$strpos];
            ++$strpos;
          }
          // Syntax check.
          if ($expect_separator) {
            throw new SyntaxError(
              "Syntax error. Expression separator expected but function name ($func) found at position $strpos in mapping '$mapping'."
            );
          }
          // Check function is valid.
          if (!array_key_exists($func, static::$availableStrFunc)) {
            // Syntax error, invalid function.
            throw new SyntaxError(
              "Syntax error. Invalid function '$func' at position $strpos in mapping '$mapping'."
            );
          }
          // Next, skip spaces.
          while (($strpos < strlen($mapping))
              && (' ' == $mapping[$strpos])
          ) {
            ++$strpos;
          }
          // We should have an opening parenthesis.
          if (($strpos >= strlen($mapping)) || ('(' != $mapping[$strpos])) {
            // Syntax error.
            throw new SyntaxError(
              "Syntax error. Expecting an opening parenthesis after function name '$func' at position $strpos in mapping '$mapping'."
            );
          }
          // Store current function name.
          $func_stack[] = $func;
          $func_concate_stack[] = $concate;
          $concate = FALSE;
          // Prepare to get its arguments.
          $func_args_stack[] = [];
          // Now we must parse function arguments.
        }
        elseif (ctype_digit($mapping[$strpos])
          || ('-' == $mapping[$strpos])
          || ('+' == $mapping[$strpos])
          || (('.' == $mapping[$strpos])
              && ($strpos + 1 < strlen($mapping))
              && ctype_digit($mapping[$strpos + 1]))
        ) {
          // Syntax check.
          if ($expect_separator) {
            throw new SyntaxError(
              "Syntax error. Expression separator expected but integer value detected at position $strpos in mapping '$mapping'."
            );
          }
          // A numeric value.
          $num_value = ('+' == $mapping[$strpos]) ? '' : $mapping[$strpos];
          ++$strpos;
          while (($strpos < strlen($mapping))
              && (ctype_digit($mapping[$strpos]) || ('.' == $mapping[$strpos]))
          ) {
            $num_value .= $mapping[$strpos];
            ++$strpos;
          }
          // Remove extra '.' if some.
          $num_value = preg_replace('#(?<=\\.)(.*?)\\.#', '\\1', $num_value);
          if (!is_numeric($num_value)) {
            throw new SyntaxError(
              "Syntax error. Failed to parse a numeric value at position $strpos in mapping '$mapping'."
            );
          }
          // Store numeric arg.
          $new_value = $num_value;
          // Go backward as we re-increment after.
          --$strpos;
          $expect_separator = TRUE;
        }
        elseif (('"' == $mapping[$strpos]) || ("'" == $mapping[$strpos])) {
          // Syntax check.
          if ($expect_separator) {
            throw new SyntaxError(
              "Syntax error. Expression separator expected but string value detected at position $strpos in mapping '$mapping'."
            );
          }
          // A quoted value.
          $quote = $mapping[$strpos];
          $str_value = '';
          ++$strpos;
          $ignore_next = FALSE;
          while (($strpos < strlen($mapping))
              && (($quote != $mapping[$strpos]) || $ignore_next)
          ) {
            $str_value .= $mapping[$strpos];
            if (('\\' == $mapping[$strpos]) && !$ignore_next) {
              $ignore_next = TRUE;
            }
            else {
              $ignore_next = FALSE;
            }
            ++$strpos;
          }
          // Store string arg.
          $new_value = $str_value;
          $expect_separator = TRUE;
        }
        elseif (',' == $mapping[$strpos]) {
          // A coma.
          if ($concate) {
            throw new SyntaxError(
              "Syntax error. Coma found at position $strpos in mapping '$mapping' while looking for another value to concate."
            );
          }
          $expect_separator = FALSE;
        }
        elseif ('.' == $mapping[$strpos]) {
          // A dot, concate.
          if ($concate) {
            throw new SyntaxError(
              "Syntax error. Dot found at position $strpos in mapping '$mapping' while looking for another value to concate."
            );
          }
          $concate = TRUE;
          $expect_separator = FALSE;
        }
        elseif (')' == $mapping[$strpos]) {
          // A closing parenthesis, resolve function call.
          if (empty($func_stack)) {
            throw new SyntaxError(
              "Syntax error. Closing parenthesis found at position $strpos in mapping '$mapping' while no corresponding function to call."
            );
          }
          $func = array_pop($func_stack);
          $args = array_pop($func_args_stack);
          try {
            // Treate special case of array.
            if ('array' == $func) {
              $value = $args;
            }
            else {
              $value = call_user_func_array($func, $args);
            }
          }
          catch (\TypeError $e) {
            throw new StringJsonPathException(
              "String function call failed for function '$func' at position $strpos in mapping '$mapping': "
              . $e
            );
          }
          catch (\Exception $e) {
            throw new StringJsonPathException(
              "String function call failed for function '$func' at position $strpos in mapping '$mapping': "
              . $e
            );
          }
          $new_value = $value;
          $concate = array_pop($func_concate_stack);
          $expect_separator = FALSE;
        }
        elseif (' ' == $mapping[$strpos]) {
          // A space.
          while (($strpos < strlen($mapping))
              && (' ' == $mapping[$strpos])
          ) {
            ++$strpos;
          }
          // Go backward as we re-increment after.
          --$strpos;
        }
        else {
          // Unprocessed.
          throw new SyntaxError(
            "Syntax error. Unexpected character '"
            . $mapping[$strpos]
            . "' at position $strpos in mapping $mapping."
          );
        }
        if (isset($new_value)) {
          if ($concate) {
            $arg_pos = count($func_args_stack[count($func_args_stack) - 1]) - 1;
            if (0 > $arg_pos) {
              throw new SyntaxError(
                "Failed to parse mapping. Empty stack for '"
                . $mapping[$strpos]
                . "' at position $strpos in mapping $mapping."
              );
            }
            $func_args_stack[count($func_args_stack) - 1][$arg_pos] .= $new_value;
            $concate = FALSE;
          }
          else {
            $func_args_stack[count($func_args_stack) - 1][] = $new_value;
          }
        }
        ++$strpos;
      }

      if (1 != count($func_args_stack)) {
        throw new StringJsonPathException(
          "Failed to extract value using mapping '$mapping' (some function arguments are reminding unused: "
          . print_r($func_args_stack, TRUE)
          . ")."
        );
      }

      $result_value = reset($func_args_stack);
      // Manage array of values to return.
      if (is_array($result_value[0])) {
        $result_value = reset($result_value);
      }
      return $result_value;
    }
    catch (StringJsonPathException $e) {
      $this->logger->error(
        "Failed to extract data: " . $e
      );
      return [];
    }
  }

  /**
   * Convert a data structure into a parsable expression.
   *
   * Get a data structure from JSONPath mapping and turns it a string that can
   * be parsed by the module.
   *
   * @param mixed $data
   *   a data structure. Non scalar or array values will be turned into string.
   *
   * @return string
   *   a parsable string expression.
   */
  protected function translateJsonToExpression($data) :string {

    if (!isset($data)) {
      $data = 'NULL';
    }
    elseif (is_string($data)) {
      $data = "'" . addslashes($data) . "'";
    }
    elseif (is_bool($data)) {
      $data = $data ? 'TRUE' : 'FALSE';
    }
    elseif (is_numeric($data)) {
      // Nothing to do.
    }
    elseif (is_array($data)) {
      $array_data = [];
      // We don't support associative array here (nor in parsing).
      foreach ($data as $subelement) {
        $array_data[] = $this->translateJsonToExpression($subelement);
      }
      $data = "array(" . implode(',', $array_data) . ")";
    }
    else {
      $data = "'" . addslashes(json_encode($data)) . "'";
    }
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function extractPropertyValuesFromRawData(
    array $raw_data,
    array &$context = []
  ) :array {
    $mapping = $this->getConfiguration()['mapping'] ?? NULL;
    if (empty($mapping)) {
      return [];
    }

    return $this->extractValueFromRawDataUsingMapping($raw_data, $mapping, $context) ?: [];
  }

  /**
   * {@inheritdoc}
   */
  public function addPropertyValuesToRawData(
    array $property_values,
    array &$raw_data,
    array &$context
  ) {
    $mapping = $this->getConfiguration()['mapping'] ?? NULL;
    if (empty($mapping)) {
      return;
    }
    // Reverse data processing.
    if (empty($context['jsonpath']['original'])) {
      $context['jsonpath']['original'] = new JsonObject(
        $context[FieldMapperInterface::CONTEXT_SOURCE_KEY] ?? []
      );
    }
    // @todo Implement.
  }

}
